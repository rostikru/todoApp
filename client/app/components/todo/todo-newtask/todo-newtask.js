import angular from 'angular';
import TodoNewtaskComponent from './todo-newtask.component';

const TodoNewtask = angular
  .module('todo.newtask', [])
  .component('todoNewtask', TodoNewtaskComponent)
  .value('EventEmitter', payload => ({ $event: payload }))
  .name;

export default TodoNewtask;
