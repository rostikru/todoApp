import template from './todo-newtask.html';
import controller from './todo-newtask.controller';
import './todo-newtask.scss';

const TodoNewtaskComponent = {
  bindings: {
    todo: '<',
    onAddTodo: '&',
  },
  controller,
  template,
};

export default TodoNewtaskComponent;
