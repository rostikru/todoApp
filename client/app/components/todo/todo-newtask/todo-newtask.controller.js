class TodoNewtaskController {
  constructor(EventEmitter) {
    this.EventEmitter = EventEmitter;
  }

  $onChanges(changes) {
    if (changes.todo) {
      this.todo = Object.assign({}, this.todo);
    }
  }
  onSubmit() {
    if (!this.todo.title) return;
    this.onAddTodo(
      this.EventEmitter({
        todo: this.todo
      })
    );
  }
}

TodoNewtaskController.$inject = ['EventEmitter'];

export default TodoNewtaskController;
