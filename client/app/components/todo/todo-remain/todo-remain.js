import angular from 'angular';
import TodoRemainComponent from './todo-remain.component';

const todoRemain = angular
  .module('todo.remain', [])
  .component('todoRemain', TodoRemainComponent)
  .name;

export default todoRemain;
