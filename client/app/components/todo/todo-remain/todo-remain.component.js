import template from './todo-remain.html';


const TodoRemainComponent = {
  bindings: {
    remain: '<'    
  },
  template,
};

export default TodoRemainComponent;
