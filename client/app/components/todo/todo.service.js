class TodoService {
  constructor($q) {
    this.$q = $q;
  }
  getTodos() {
    return this.$q.when([
      {
        title: 'Make Plan of work',
        done: false,
      },
      {
        title: 'Choose harder task',
        done: false,
      },
        {
        title: 'Start make it',
        done: false,
      },
    ]);
  }
}

TodoService.$inject = ['$q'];

export default TodoService;
