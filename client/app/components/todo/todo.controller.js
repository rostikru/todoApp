class TodoController {
  constructor(TodoService) {
    this.todoService = TodoService;
  }
  $onInit() {
    this.newTodo = {
      title: '',
      done: false,
    };
    this.todos = [];
    this.todoService.getTodos().then((response) => {
      this.todos = response;
      this.len = response.length;
    });
  }
  $onChanges(changes) {
    if (changes.todoData) {
      this.todos = Object.assign({}, this.todoData);
    }
  }
  addTodo({ todo }) {
    if (!todo) return;
    this.todos.push(todo);
    this.newTodo = {
      title: '',
      done: false,
    };
  }
  removeTodo({ index }) {
    this.todos.splice(index, 1);
  }
  archiveTodos() {
    this.todos = this.todos.filter(todo => todo.done === false);
  }
  remaining() {
    this.len = this.todos.length - this.todos.filter(todo => todo.done === true).length;
    return this.len;
  }
 }

TodoController.$inject = ['TodoService'];

export default TodoController;
