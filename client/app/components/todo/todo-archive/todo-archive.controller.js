class TodoArchiveController {
  constructor(EventEmitter) {
    this.EventEmitter = EventEmitter;
  }
  $onChanges(changes) {
    if (changes.todo) {
      this.todo = Object.assign({}, this.todo);
    }
  }
  onArchive() {
    this.onArchiveTodos(
      this.EventEmitter({
        todos: this.todos
      })
    );
  }
}

TodoArchiveController.$inject = ['EventEmitter'];

export default TodoArchiveController;
