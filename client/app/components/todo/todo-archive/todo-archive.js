import angular from 'angular';
import TodoArchiveComponent from './todo-archive.component';

const todoArchive = angular
  .module('todo.archive', [])
  .component('todoArchive', TodoArchiveComponent)
  .value('EventEmitter', payload => ({ $event: payload }))
  .name;

export default todoArchive;
