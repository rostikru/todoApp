import template from './todo-archive.html';
import controller from './todo-archive.controller';
import './todo-archive.scss';


const TodoArchiveComponent = {
  bindings: {
    todos: '<',
    onArchiveTodos: '&',
  },
  controller,
  template,
};

export default TodoArchiveComponent;
