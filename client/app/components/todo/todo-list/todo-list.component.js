import template from './todo-list.html';
import controller from './todo-list.controller';
import './todo-list.scss';

const TodoListComponent = {
  bindings: {
    todos: '<',
    onRemoveTodo: '&',
  },
  controller,
  template,
};

export default TodoListComponent;
