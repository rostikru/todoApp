class TodoListController {
  constructor(EventEmitter) {
    this.EventEmitter = EventEmitter;
  }
  $onChanges(changes) {
    if (changes.todo) {
      this.todo = Object.assign({}, this.todo);
    }
  }
  onRemove(index) {
    this.onRemoveTodo(
      this.EventEmitter({
        index: index,
      }),
    );
  }
}

TodoListController.$inject = ['EventEmitter'];

export default TodoListController;
