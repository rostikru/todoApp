import angular from 'angular';
import uiRouter from 'angular-ui-router';
import TodoComponent from './todo.component';
import TodoService from './todo.service';
import TodoNewtask from './todo-newtask/todo-newtask';
import TodoRemain from './todo-remain/todo-remain';
import TodoList from './todo-list/todo-list';
import TodoArchive from './todo-archive/todo-archive';

const todoModule = angular.module('todo', [
  uiRouter,
  TodoNewtask,
  TodoArchive,
  TodoList,
  TodoRemain
])
  .config(($stateProvider) => {
    'ngInject';

    $stateProvider
      .state('todo', {
        url: '/todo',
        component: 'todo',
      });
  })

  .component('todo', TodoComponent)
  .service('TodoService', TodoService)
  .name;

export default todoModule;
